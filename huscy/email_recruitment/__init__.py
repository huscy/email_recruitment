# major.minor.patch.release.number
# release must be one of alpha, beta, rc or final
VERSION = (0, 4, 0, 'alpha', 7)

__version__ = '.'.join(str(x) for x in VERSION)
